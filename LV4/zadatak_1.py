import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn . model_selection import train_test_split

data=pd.read_csv("data_C02_emission.csv")
input_variables=['Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)','Engine Size (L)','Cylinders']
output_variable=['CO2 Emissions (g/km)']
X=data[input_variables].to_numpy()
Y=data[output_variable].to_numpy()
X_train,X_test,Y_train,Y_test=train_test_split(X,Y,test_size=0.2,random_state=1)

plt.scatter(X_train[:,0],Y_train,c="blue")
plt.scatter(X_test[:,0],Y_test,c="red")
plt.xlabel("Numerical values")
plt.ylabel("CO2 Emissions")
plt.show()
